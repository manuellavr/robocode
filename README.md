# Robô AdaBot42 - Solutis

## Movimentação
O comportamento do robô é relativamente simples: a movimentação base é circular, rotacionando sempre 45 graus à direita e o robô gira sua arma em 360 graus junto ao radar, buscando outros robôs.

## Mira
O robô usa uma estratégia de mira que pressupõe que o oponente vai se mexer de forma linear de acordo com a direção para a qual ele está virado e a velocidade com a qual está andando. Esse comportamento é alcançado, inicialmente, encontrando-se a posição do robô oponente através da distância e do seu ângulo até ele. Depois, fazemos uma estimativa do tempo que a bala levaria até atingir o oponente através da fórmula: tempo = distância/velocidade, onde a velocidade neste caso seria a velocidade da bala. A partir daí é possível calcular novos pontos de X e Y futuros com base no ângulo para o qual o robô oponente está indo (heading) e a distância encontrada. Esse processo pode ser visto no bloco de código abaixo:

``` java
double enemyAngle = getHeading() + e.getBearing() % 360;
otherRobotX = Math.sin(Math.toRadians(enemyAngle)) * e.getDistance() + getX();
otherRobotY = Math.cos(Math.toRadians(enemyAngle)) * e.getDistance() + getY();
		
predictedX = Math.sin(e.getHeadingRadians()) * e.getVelocity() * timeTillBulletHits(e.getDistance(), currentFirePower) + otherRobotX;
predictedY = Math.cos(e.getHeadingRadians()) * e.getVelocity() * timeTillBulletHits(e.getDistance(), currentFirePower) + otherRobotY;
```
Depois de encontrar o local onde o robô oponente supostamente estará depois do tempo passado, calculo o ângulo até esse ponto e a arma do meu robô é rotacionada para essa posição.

Essa categoria de mira não é perfeita, porque faz uma suposição que pode não ser verdadeira (ex.: o robô pode estar se mexendo de forma circular, não linear; ou pode mudar de direção rapidamente), ainda assim é uma boa aproximação para distâncias curtas e contra robôs que se movem de forma linear. 

## Tiro

A força do tiro é definida pelo método chooseFirePower() de acordo com alguns fatores: a distância até o oponente, a energia restante do oponente e a energia restante do meu robô. As regras de escolha da força são as seguintes: 

- Para uma distância menor que 100:

`Math.min(enemyEnergy/4, getEnergy() > 3 ? 3 : getEnergy()/4);` 

 esse trecho escolhe o valor mínimo entre dois valores possíveis: o primeiro é a energia do oponente dividida por 4 (dado que o dano que a bala faz ao oponente é igual a 4*força). O segundo valor é variável de acordo com a energia do meu robô, se for maior do que 3 (ou seja, se eu posso atirar uma bala de força 3 sem que isso desative o meu robô), então este valor é 3, caso contrário, será a minha energia divida por 4. O objetivo esperado é que o meu robô possa recuperar energia aos poucos, nunca atirando com uma força maior do que precisa/pode. 

 Para distâncias maiores que 100 e menores que 200, o segundo parâmetro em Math.min() tem o 3 substituido por 2. E para distâncias maiores que 300, esse valor é substituído por 1. Como a força da bala influencia na sua velocidade e é mais difícil que o tiro acerte estando a uma distância maior, o valor do tiro é reduzido a altas distâncias para evitar gastar muita energia com um tiro que talvez não alcance o oponente.

Também existe um método chamado shouldIShoot(), que ajuda a decidir se vale a pena atirar nesse turno. Em geral o robô só é indicado a não atirar se a distância até o inimigo é muito grande e a energia do meu robô é muito baixa ou se os últimos 3 tiros erraram o alvo e a distância é alta. Essas condições visam evitar que o robô atire de forma desnecessária.  

## Desvio da Parede

Como o robô é do tipo AdvancedRobot, é preciso evitar bater nas paredes para não tomar dano. Criei uma condição que informa se o robô está ou não próximo a uma parede, conferindo se sua posição está a menos de 80 pixels de alguma das bordas do campo. O código lança um evento customizado quando o robô está perto da parede e nesse evento faço com que o robô vá para o meio do campo de batalha. Como o robô se move em círculos, é ideal que ele fique sempre próximo ao centro para evitar bater nas bordas.  

O código para fazer com que o robô vá até o centro do campo é parecido com o código de mira. Encontro a distância do ponto onde o meu robô está até o meio do campo e faço com que ele se vire na direção correta `setTurnRight(anguloAteOCentro)` e ande a distância necessária `setAhead(distanciaAteOCentro)`. 

## Visão Geral

O robô lida bem com a questão de escolha de força do tiro e se sai bem em batalhas com muitos robôs (ao menos os disponibilizados pelo robocode). A depender do oponente, não se sai bem em batalhas 1vs1, em parte por conta da movimentação que no momento está limitada a ser apenas circular. Em geral há algumas coisas que podem ser aprimoradas, como o código de mira, que poderia considerar movimentos não lineares ou armazenar informações sobre o movimento dos inimigos durante a partida e analisar a melhor estratégia a se usar. O método de lidar com a proximidade à parede também poderia ser ajustado, afinal isso leva o meu robô a se mover linearmente até o centro, o que facilita com que outros robôs o atinjam durante esse tempo. 

Foi interessante aprender a usar o robocode, é uma ferramenta desafiadora. Me ajudou a exercitar as habilidades não só de programação mas também de trigonometria e gostaria de aprimorar o robô futuramente.   

 



