package mv;
import robocode.*;
import java.awt.Color;
import java.awt.Graphics2D;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * AdaBot42 - um robo por Manuella Vieira
 */

public class AdaBot extends AdvancedRobot
{
	
	private static int WALL_DISTANCE = 80;	
	
	/*
	 * Variveis de controle
	 */
	private int bulletsMissed = 0, bulletsHit = 0, hitWall = 0;
	private boolean oneOnOne = false;
	private boolean toggleEvent = false;
	private int movementDirection = 1;
	private int turnsWithoutShooting = 0;
	
	private double otherRobotX, otherRobotY;
	private double predictedX, predictedY;
	private boolean[] missingTooMany = {false, false, false}; 
	private int idxMiss = 0;
	
	/**
	 * Condicoes customizadas
	 */
	private Condition conditionWall = new Condition("isNearWall"){ 
		public boolean test()
    	{
	        return isCloseToWall();
	    } 
	};
	
	public void run() {
	
		oneOnOne = getOthers() == 1 ? true : false;
		setColors(Color.BLACK,Color.MAGENTA,Color.YELLOW);
		setBulletColor(Color.CYAN);
		setAdjustGunForRobotTurn(true);
		
		addCustomEvent(conditionWall);

		while(true) {
				move();
		}
	}
	
	private void move(){
			setAhead(300 * movementDirection);
			setTurnGunRight(360);
			setTurnRight(45);
			execute();
	}
	
	/*
	 * Lida com o evento de o robo estar proximo a parede
	 */
	public void onCustomEvent(CustomEvent e){
	
		if(e.getCondition().getName().equals("isNearWall")){
				toggleEvent = toggleEvent ? false : true;
					if(toggleEvent) {
						setMaxVelocity(Rules.MAX_VELOCITY);
						movementDirection *= -1;
						setAhead(30 * movementDirection);
						waitFor(new MoveCompleteCondition(this));
						goToCenter();
						toggleEvent = false;
					}
			}
	}
	
	/*
	 * Vai para o centro do campo
	 */
	public void goToCenter() {
		
		double XCenter = getBattleFieldWidth()/2;
		double YCenter = getBattleFieldHeight()/2;
		
		double angleToCenter = robocode.util.Utils.normalRelativeAngleDegrees(Math.toDegrees(Math.atan2(XCenter - getX(), YCenter - getY())) - getHeading());
		double distanceToCenter =  Math.hypot(XCenter - getX(), YCenter - getY());
		
		int moveDir = movementDirection;
	
		if((angleToCenter >= 90 && angleToCenter <= 180) || (angleToCenter >= -180 && angleToCenter <= -90)) {
			moveDir = -1 * moveDir;
		}
		setTurnRight(angleToCenter);
		waitFor(new TurnCompleteCondition(this));
		setAhead(distanceToCenter * moveDir);
		waitFor(new MoveCompleteCondition(this));
		
	}
	
	/*
	 * Mira a arma linearmente de acordo com a velocidade e direcao do robo oponente
	 */
	public void onScannedRobot(ScannedRobotEvent e){
		
		double currentFirePower = chooseFirePower(e.getEnergy(), e.getDistance());
		double enemyAngle = getHeading() + e.getBearing() % 360;
		
		otherRobotX = Math.sin(Math.toRadians(enemyAngle)) * e.getDistance() + getX();
		otherRobotY = Math.cos(Math.toRadians(enemyAngle)) * e.getDistance() + getY();
		
		predictedX = Math.sin(e.getHeadingRadians()) * e.getVelocity() * timeTillBulletHits(e.getDistance(), currentFirePower) + otherRobotX;
		predictedY = Math.cos(e.getHeadingRadians()) * e.getVelocity() * timeTillBulletHits(e.getDistance(), currentFirePower) + otherRobotY;
		
		if(predictedX > getBattleFieldWidth())
			predictedX = getBattleFieldWidth();
		if(predictedY > getBattleFieldHeight())
			predictedY = getBattleFieldHeight();
		if(predictedX < 0)
			predictedX = 0;
		if(predictedY < 0)
			predictedY = 0;
		
		double newBearing = Math.toDegrees(Math.atan2(predictedX - getX(), predictedY - getY()));
		
		if(shouldIShoot(e.getDistance(), e.getEnergy())) {
			setMaxVelocity(4);
			double shootingAngle = robocode.util.Utils.normalRelativeAngleDegrees(newBearing - getGunHeading()); 
			setTurnGunRight(shootingAngle);
			waitFor(new GunTurnCompleteCondition(this));			
			setFire(currentFirePower);
			setMaxVelocity(8);
			
			turnsWithoutShooting = 0;
		}
		else {
			turnsWithoutShooting++;
		}
		if(oneOnOne) {
			setTurnRight(e.getBearing());
			setAhead(e.getDistance()/2);
		}
	}
	
	/*
	 * Define a fora do tiro com base na energia e na distancia do oponente 
	 */
	
	public double chooseFirePower(double enemyEnergy, double enemyDistance) {
		
		if(enemyDistance <= 100) {
			return Math.min(enemyEnergy/4, getEnergy() > 3 ? 3 : getEnergy()/4);
		}
		else if(enemyDistance <= 200) {
			return Math.min(enemyEnergy/4, getEnergy() > 2 ? 2 : getEnergy()/4);
		}
		else 
			return Math.min(enemyEnergy/4, getEnergy() >= 1 ? 1 : getEnergy()/4);
	}
	
	/*
	 * Define se e vantajoso atirar com base na distancia e energia do oponente e na energia restante do meu robo
	 */
	public boolean shouldIShoot(double enemyDistance, double enemyEnergy) {
		
		double minEnergy = oneOnOne ? 21 : 30; 
		
		if(turnsWithoutShooting >= 10 && getEnergy() >= 16 && oneOnOne)
			return true;
		if(enemyDistance > 400 && !oneOnOne) 
			return false;
		if(enemyDistance > 300 && getEnergy() <= minEnergy)
			return false;
		else if(oneOnOne && ((missingTooMany[0] && missingTooMany[1] && missingTooMany[2]) && enemyDistance >= 300))
			return false;
		else
			return true;
	}
	
	/*
	 * Se o robo oponente estiver na frente da minha arma, atira 
	 */
	public void onHitRobot(HitRobotEvent e) {
	
		double shootingAngle = e.getBearing() + getHeading() - getGunHeading();
		if(Math.abs(shootingAngle) <= 20) {
			setTurnGunRight(shootingAngle);
			waitFor(new GunTurnCompleteCondition(this));
			setFire(chooseFirePower(e.getEnergy(), 0));
		}
		if(ehPrimeiroQuadrante(e.getBearing()) || ehSegundoQuadrante(e.getBearing()))
		{
			setBack(150);
		}
		else
			setAhead(150);		
		setTurnRight(90);
	}
	
	public void onHitByBullet(HitByBulletEvent e) 
	{
		int moveDir = 1;
		if((e.getBearing() >= -45 && e.getBearing() <= 45) || (e.getBearing() <= -150 && e.getBearing() >= 150))
			setTurnRight(90);
		if(ehPrimeiroQuadrante(e.getBearing()) || ehSegundoQuadrante(e.getBearing()))
			moveDir = -1;
		setAhead(100 * moveDir);
	}
	
	public void onBulletMissed(BulletMissedEvent e) {
		bulletsMissed++;
		missingTooMany[idxMiss] = true;
		idxMiss = idxMiss == 2 ? 0 : idxMiss + 1;
	}
	
	public void onBulletHit(BulletHitEvent e) {
		bulletsHit++;
		missingTooMany[idxMiss] = false;
		idxMiss = idxMiss == 2 ? 0 : idxMiss + 1;
	}
	
	public void onHitWall(HitWallEvent e) {
		hitWall++;
	}
	
	public void onRoundEnded(RoundEndedEvent event) {
		debugBullets();
	}
	 
	public void onRobotDeath(RobotDeathEvent event) {
		oneOnOne = getOthers() == 1 ? true : false;
	}

	/*
	 * Mtodos auxiliares, retornam se um ngulo x est em um determinado quadrante 
	 */
	 
	public boolean ehPrimeiroQuadrante(double x){
		if(x >= 0 && x <= 90)
			return true;
		return false; 
	}
	
	public boolean ehSegundoQuadrante(double x){
		if(x < 0) 
			x += 360;
		if(x <= 360 && x >= 270)
			return true;
		return false; 
	}
	
	public boolean ehTerceiroQuadrante(double x){
		if(x < 0) 
			x += 360;
		if( x >= 180 && x <= 270)
			return true;
		return false; 
	}
	
	public boolean ehQuartoQuadrante(double x){
		if(x >= 90 && x <= 180)
			return true;
		return false; 
	}
	
	/*
	 * Confere se o robo esta muito proximo da parede
	 */
	public boolean isCloseToWall(){
		if(getY() >= getBattleFieldHeight() - WALL_DISTANCE  || getY() <= WALL_DISTANCE || getX() >= getBattleFieldWidth() - WALL_DISTANCE || getX() <= WALL_DISTANCE){
			return true;
		}
		return false;
	}
	
	/*
	 * Calcula o tempo que a bala levaria at atingir o oponente
	 */
	public double timeTillBulletHits(double distance, double firePower){
		return distance/(20 - (3*firePower));
	}
	
	/*
	 * Mtodos para depurao
	 */
	
	public void debugTargeting(ScannedRobotEvent e){
		out.println("Robot Heading: " + getHeading());
		out.println("Gun Heading: " + getGunHeading());
		out.println("Other Robot Bearing: " + e.getBearing() + "\n");
	}

	public void debugBullets(){	
		out.println("Bullets Hit: " + bulletsHit + " Bullets Missed: " + bulletsMissed + "\n\n");
	}
	
	public void debugWallAvoidance() {
		out.println("Lidando com evento... andando para: " + (movementDirection == 1 ? "frente" : "trs"));
	}
	
	 public void onPaint(Graphics2D g)
     {
		 g.setColor(new Color(0xff, 0x00, 0x00, 0x80));
		  
		 g.drawLine((int)getX(), (int)getY(), (int)predictedX, (int)predictedY);
		    
     }
}
